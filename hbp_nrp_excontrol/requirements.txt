rospkg==1.2.10
pyxb==1.2.6
catkin_pkg==0.4.23
PyYAML==5.4
RestrictedPython==3.6.0; python_version=="2.7"
RestrictedPython>=4.0; python_version >= "3.0"
tf
numpy==1.21

