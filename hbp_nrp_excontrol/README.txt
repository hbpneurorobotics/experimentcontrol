This package contains the experiment control of experiments running in the HBP Neurorobotics platform.

The experiment control provides an interface to the HBP NRP backend to control a simulation. Through
this interface, the state machines of a simulation can be controlled.
