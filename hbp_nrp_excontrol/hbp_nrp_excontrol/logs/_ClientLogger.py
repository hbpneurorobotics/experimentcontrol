# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
Logger to be used in Transfer Functions
"""
# pylint: disable=redefined-builtin
from builtins import map
from builtins import object

import rospy

from cle_ros_msgs.msg import ClientLoggerMessage
from hbp_nrp_excontrol.__internals import LOG_PUB


class ClientLogger(object):
    """
    Object that can be used to send log messages to the ROS topic subscribed by the client
    """

    def __init__(self):
        self.__log_publisher = rospy.Publisher(LOG_PUB, ClientLoggerMessage, queue_size=10)

    def log(self, loglevel, *objs, **kwargs):
        """
        Logs a message

        :param loglevel: the log level (eg, INFORMATION, ADVERTISEMENT)
        :param objs: objects to log
        :param sep: overrides the separator default value (i.e. ' ')
        :param duration: log duration in ms when applicable, overrides default value (i.e. 5000ms)
        """
        sep = kwargs.get('sep', ' ')
        duration = kwargs.get('duration', 5000)
        clm = ClientLoggerMessage(loglevel, sep.join(map(str, objs)), duration)
        self.__log_publisher.publish(clm)

    def info(self, *objs, **kwargs):
        """
        Logs a message with severity level 'Info'

        :param objs: objects to log
        :param sep: overrides the separator default value (i.e. ' ')
        """
        self.log(ClientLoggerMessage.INFORMATION, *objs, **kwargs)

    def advertise(self, *objs, **kwargs):
        """
        Logs a message with severity level 'Advertise'

        :param objs: objects to log
        :param sep: overrides the separator default value (i.e. ' ')
        """
        self.log(ClientLoggerMessage.ADVERTISEMENT, *objs, **kwargs)
