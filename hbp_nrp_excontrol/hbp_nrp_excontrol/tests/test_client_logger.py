# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
from hbp_nrp_excontrol.logs._ClientLogger import ClientLogger
from cle_ros_msgs.msg import ClientLoggerMessage
from unittest.mock import Mock, patch
import unittest


class TestClientLogger(unittest.TestCase):
    def setUp(self):
        self.rospy_patch = patch("hbp_nrp_excontrol.logs._ClientLogger.rospy")
        rospy_mock = self.rospy_patch.start()
        self.publisher_mock = rospy_mock.Publisher
        self.publisher_mock.return_value = Mock()
        self.clientLogger = ClientLogger()

    def tearDown(self):
        self.rospy_patch.stop()

    def test_string_conversion(self):
        self.clientLogger.info('bla bla')
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, 'bla bla', 5000))

        self.clientLogger.info(3)
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, '3', 5000))

        self.clientLogger.info((2, 3))
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, '(2, 3)', 5000))

        self.clientLogger.info([2, 3])
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, '[2, 3]', 5000))

        self.clientLogger.info({'id': 'value'})
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, "{'id': 'value'}", 5000))

    def test_multiple_params(self):
        self.clientLogger.info(2, 3, 4)
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, '2 3 4', 5000))

        self.clientLogger.info("a", 2, (2, 3))
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, 'a 2 (2, 3)', 5000))

    def test_separator(self):
        self.clientLogger.info(2, 3, 4, sep=',')
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, '2,3,4', 5000))

        self.clientLogger.info(2, 3, 4, sep='==')
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, '2==3==4', 5000))

    def test_log_levels(self):
        self.clientLogger.info('a')
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.INFORMATION, 'a', 5000))

        self.clientLogger.advertise('a')
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.ADVERTISEMENT, 'a', 5000))

    def test_log_duration(self):
        self.clientLogger.advertise('test')
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.ADVERTISEMENT, 'test', 5000))

        self.clientLogger.advertise('test', duration=3000)
        self.publisher_mock.return_value.publish.assert_called_with(
            ClientLoggerMessage(ClientLoggerMessage.ADVERTISEMENT, 'test', 3000))
