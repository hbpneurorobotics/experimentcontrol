# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
__author__ = 'Georg Hinkel'

import unittest
from hbp_nrp_excontrol.nrp_states import (WaitToClockState, ClockDelayState, LightServiceState,
                                          SetMaterialColorServiceState, RobotPoseMonitorState,
                                          LinkPoseMonitorState, RobotTwistMonitorState,
                                          WarningState, MonitorSpikeRateState,
                                          MonitorLeakyIntegratorAlphaState,
                                          MonitorLeakyIntegratorExpState, MonitorSpikeRecorderState,
                                          SpawnSphere, SpawnCylinder, SpawnBox,
                                          DestroyModel, SetModelPose, TransformModelState,
                                          TranslateModelState, RotateModelState,
                                          ScaleModelState, StopSimulation)
import rospy
import hbp_nrp_excontrol as exc
from std_msgs.msg import ColorRGBA
from rosgraph_msgs.msg import Clock
from std_srvs.srv import Empty
from gazebo_msgs.msg import ModelStates, LinkStates
from geometry_msgs.msg import Pose, Twist, Point, Quaternion, Vector3
from unittest.mock import patch, Mock
from cle_ros_msgs.msg import SpikeRate, SpikeEvent, SpikeData


def _set_warning_message(msg):
    TestStates.warning_message = msg


class TestStates(unittest.TestCase):

    def test_clock_condition(self):
        test = WaitToClockState(42)
        self.assertEqual("/clock", test._topic)
        self.assertEqual(Clock, test._msg_type)
        # Comparison operator does not work with Mock.
        # So let's use an actual rospy.Time.
        c = Clock(rospy.Time(40))
        self.assertTrue(test._cond_cb(None, c))
        c = Clock(rospy.Time(42))
        self.assertFalse(test._cond_cb(None, c))

    def test_clock_condition_float(self):
        test = WaitToClockState(1.5)
        # Before threshold. Return True
        c = Clock(rospy.Time(1, 400000000))
        self.assertAlmostEqual(c.clock.to_sec(), 1.4, places=10)
        self.assertTrue(test._cond_cb(None, c))
        # After Treshold ...
        c = Clock(rospy.Time(1, 600000000))
        self.assertAlmostEqual(c.clock.to_sec(), 1.6, places=10)
        self.assertFalse(test._cond_cb(None, c))

    def test_clock_delay_state_rospyTime(self):
        test = ClockDelayState(rospy.Time.from_sec(1.5))
        c = Clock(rospy.Time(1, 1000000))
        # Remembers start time.
        self.assertTrue(test._cond_cb(None, c))
        c = Clock(rospy.Time(2, 500000000))
        # Just before delay duration ends.
        self.assertTrue(test._cond_cb(None, c))
        c = Clock(rospy.Time(2, 600000001))
        # After delay duration (2.60000001 - start_time=1.1 > delay = 1.5sec)
        self.assertFalse(test._cond_cb(None, c))

    @patch("hbp_nrp_excontrol.nrp_states._LightServiceState.rospy")
    def test_light_service_state(self, rospy):
        rospy.is_shutdown.return_value = False
        light_state = LightServiceState("vr_lamp", diffuse={'r': 0.4, 'g': 0.1, 'b': 0.7, 'a': 1.0},
                                        attenuation_constant=42)
        self.assertEqual("/gazebo/get_light_properties", light_state._service_name)
        light = Mock()
        light.diffuse = ColorRGBA(1, 0, 1, 1)
        light.attenuation_constant = 2
        light.attenuation_linear = 0.4
        light.attenuation_quadratic = 0.8
        rospy.ServiceProxy().return_value = light
        request = light_state.create_service_request()
        self.assertEqual(42, request.attenuation_constant)
        self.assertEqual(0.4, request.attenuation_linear)
        self.assertEqual(0.8, request.attenuation_quadratic)
        self.assertIsInstance(request.diffuse, ColorRGBA)
        self.assertEqual("vr_lamp", request.light_name)

        light_state_2 = LightServiceState("vr_lamp", attenuation_linear=0.6,
                                          attenuation_quadratic=0.1)
        request = light_state_2.create_service_request()
        self.assertEqual(2, request.attenuation_constant)
        self.assertEqual(0.6, request.attenuation_linear)
        self.assertEqual(0.1, request.attenuation_quadratic)
        self.assertIsInstance(request.diffuse, ColorRGBA)
        self.assertEqual("vr_lamp", request.light_name)

    def test_material_service(self):
        test = SetMaterialColorServiceState("left_vr_screen", "body", "screen_glass", "Gazebo/Red")
        self.assertEqual('/gazebo/set_visual_properties', test._service_name)
        test_request = test._request
        self.assertEqual("left_vr_screen", test_request.model_name)
        self.assertEqual("body", test_request.link_name)
        self.assertEqual("screen_glass", test_request.visual_name)
        self.assertEqual("material:script:name", test_request.property_name)
        self.assertEqual("Gazebo/Red", test_request.property_value)

    def test_destroy_model_service(self):
        test = DestroyModel("some_name")
        self.assertEqual('/gazebo/delete_model', test._service_name)
        test_request = test._request
        self.assertEqual("some_name", test_request)

    def test_destroy_model_service_callback(self):
        test = DestroyModel(lambda ud, _: "some_name")
        self.assertEqual('/gazebo/delete_model', test._service_name)
        test_request = test._request_cb(None, None)
        self.assertEqual("some_name", test_request)

    def test_model_state_service(self):
        test = SetModelPose("some_name", Vector3(1, 2, 0.3), Vector3(0, 0, 0), Vector3(4, 0.1, 5))
        self.assertEqual('/gazebo/set_model_state', test._service_name)
        test_request = test._request
        self.assertEqual("some_name", test_request.model_name)
        self.assertEqual(Point(1, 2, 0.3), test_request.pose.position)
        self.assertEqual(Quaternion(0, 0, 0, 1), test_request.pose.orientation)
        self.assertEqual(Vector3(4, 0.1, 5), test_request.scale)
        self.assertEqual("world", test_request.reference_frame)

    def test_model_state_service_callback(self):
        test = SetModelPose(lambda ud, _: "some_name", Vector3(1, 2, 0.3), Vector3(0, 0, 0),
                            Vector3(4, 0.1, 5))
        self.assertEqual('/gazebo/set_model_state', test._service_name)
        test_request = test._request_cb(None, None)
        self.assertEqual("some_name", test_request.model_name)
        self.assertEqual(Point(1, 2, 0.3), test_request.pose.position)
        self.assertEqual(Quaternion(0, 0, 0, 1), test_request.pose.orientation)
        self.assertEqual(Vector3(4, 0.1, 5), test_request.scale)
        self.assertEqual("world", test_request.reference_frame)

    @patch("hbp_nrp_excontrol.nrp_states._ModelServiceState.rospy")
    def test_movement_state_service(self, rospy):
        rospy.is_shutdown.return_value = False
        test = TranslateModelState("some_name", 1, 2, 0.3)
        self.assertEqual('/gazebo/set_model_state', test._service_name)

        model = Mock()
        model.model_name = "some_name"
        model.pose.position = Point(1, -1, 1)
        model.pose.orientation = Quaternion(0, 0, 0, 1)
        model.scale = Vector3(1, 1, 1)
        rospy.ServiceProxy().return_value = model
        test_request = test.create_service_request()

        self.assertEqual("some_name", test_request.model_state.model_name)
        self.assertEqual(test_request.model_state.pose.position, Point(2, 1, 1.3))
        self.assertEqual(test_request.model_state.pose.orientation, Quaternion(0, 0, 0, 1))
        self.assertEqual(test_request.model_state.scale, Vector3(1, 1, 1))

        piHalf = 1.5707963267948966
        test = RotateModelState("some_name", piHalf, -piHalf, piHalf)
        self.assertEqual('/gazebo/set_model_state', test._service_name)
        rospy.ServiceProxy().return_value = model
        test_request = test.create_service_request()
        self.assertEqual("some_name", test_request.model_state.model_name)
        self.assertEqual(test_request.model_state.pose.position, Point(2, 1, 1.3))
        self.assertAlmostEqual(test_request.model_state.pose.orientation.x, 0.70710678118654746)
        self.assertAlmostEqual(test_request.model_state.pose.orientation.y, 0)
        self.assertAlmostEqual(test_request.model_state.pose.orientation.z, 0.70710678118654746)
        self.assertAlmostEqual(test_request.model_state.pose.orientation.w, 0)
        self.assertEqual(test_request.model_state.scale, Vector3(1, 1, 1))

        test = ScaleModelState("some_name", 1, 2, 3)
        self.assertEqual('/gazebo/set_model_state', test._service_name)
        rospy.ServiceProxy().return_value = model
        test_request = test.create_service_request()
        self.assertEqual("some_name", test_request.model_state.model_name)
        self.assertEqual(test_request.model_state.pose.position, Point(2, 1, 1.3))
        self.assertAlmostEqual(test_request.model_state.pose.orientation.x, 0.70710678118654746)
        self.assertAlmostEqual(test_request.model_state.pose.orientation.y, 0)
        self.assertAlmostEqual(test_request.model_state.pose.orientation.z, 0.70710678118654746)
        self.assertAlmostEqual(test_request.model_state.pose.orientation.w, 0)
        self.assertEqual(test_request.model_state.scale, Vector3(1, 2, 3))

    def test_spawn_sphere_service(self):
        can_collide = True
        use_gravity = False
        is_kinematic = True
        test = SpawnSphere("some_name", 0.33, Vector3(1, 2, 3), Vector3(0, 0, 0), can_collide,
                           use_gravity, is_kinematic)
        self.assertEqual('/gazebo/spawn_sdf_entity', test._service_name)
        test_request = test._request
        self.assertEqual("some_name", test_request.entity_name)
        initialPose = Pose(Point(1, 2, 3), Quaternion(0, 0, 0, 1))
        self.assertEqual(initialPose, test_request.initial_pose)
        self.assertEqual("world", test_request.reference_frame)

        self.assertIn("<sphere><radius>0.33</radius></sphere>", test_request.entity_xml)
        self.assertIn("<collision name=", test_request.entity_xml)
        self.assertIn("</collision>", test_request.entity_xml)
        self.assertIn("<kinematic>1</kinematic>", test_request.entity_xml)
        self.assertIn("<gravity>0</gravity>", test_request.entity_xml)

    def test_spawn_sphere_with_callback_name(self):
        can_collide = True
        use_gravity = True
        is_kinematic = False
        test = SpawnSphere(lambda ud, _: "some_name", 0.33, lambda ud, _: Vector3(1, 2, 3),
                           Vector3(0, 0, 0), can_collide, use_gravity, is_kinematic)
        self.assertEqual('/gazebo/spawn_sdf_entity', test._service_name)
        test_request = test._request_cb(None, None)
        self.assertEqual("some_name", test_request.entity_name)
        initialPose = Pose(Point(1, 2, 3), Quaternion(0, 0, 0, 1))
        self.assertEqual(initialPose, test_request.initial_pose)
        self.assertEqual("world", test_request.reference_frame)

        self.assertIn("<sphere><radius>0.33</radius></sphere>", test_request.entity_xml)
        self.assertIn("<collision name=", test_request.entity_xml)
        self.assertIn("</collision>", test_request.entity_xml)
        self.assertIn("<kinematic>0</kinematic>", test_request.entity_xml)
        self.assertIn("<gravity>1</gravity>", test_request.entity_xml)

    def test_box_sphere_service(self):
        can_collide = False
        use_gravity = True
        is_kinematic = False
        test = SpawnBox("some_name", Vector3(0.1, 0.2, 0.3), Vector3(1, 2, 3), Vector3(0, 0, 0),
                        can_collide, use_gravity, is_kinematic)
        self.assertEqual('/gazebo/spawn_sdf_entity', test._service_name)
        test_request = test._request
        self.assertEqual("some_name", test_request.entity_name)
        initialPose = Pose(Point(1, 2, 3), Quaternion(0, 0, 0, 1))
        self.assertEqual(initialPose, test_request.initial_pose)
        self.assertEqual("world", test_request.reference_frame)
        self.assertIn("<box><size>0.1 0.2 0.3</size></box>", test_request.entity_xml)
        self.assertNotIn("<collision>", test_request.entity_xml, )
        self.assertIn("<kinematic>0</kinematic>", test_request.entity_xml)
        self.assertIn("<gravity>1</gravity>", test_request.entity_xml)

    def test_box_sphere_service_callable_name(self):
        can_collide = False
        use_gravity = True
        is_kinematic = False
        test = SpawnBox(lambda ud, _: "some_name", Vector3(0.1, 0.2, 0.3), Vector3(1, 2, 3),
                        Vector3(0, 0, 0), can_collide, use_gravity, is_kinematic)
        self.assertEqual('/gazebo/spawn_sdf_entity', test._service_name)
        test_request = test._request_cb(None, None)
        self.assertEqual("some_name", test_request.entity_name)
        initial_pose = Pose(Point(1, 2, 3), Quaternion(0, 0, 0, 1))
        self.assertEqual(initial_pose, test_request.initial_pose)
        self.assertEqual("world", test_request.reference_frame)
        self.assertIn("<box><size>0.1 0.2 0.3</size></box>", test_request.entity_xml)
        self.assertNotIn("<collision>", test_request.entity_xml, )
        self.assertIn("<kinematic>0</kinematic>", test_request.entity_xml)
        self.assertIn("<gravity>1</gravity>", test_request.entity_xml)

    def test_spawn_cylinder_service(self):
        can_collide = False
        use_gravity = False
        is_kinematic = False
        test = SpawnCylinder("some_name", 100, 200.5, Vector3(1, 2, 3),
                             Vector3(0, 0, 0), can_collide, use_gravity, is_kinematic)
        self.assertEqual('/gazebo/spawn_sdf_entity', test._service_name)
        test_request = test._request
        self.assertEqual("some_name", test_request.entity_name)
        initial_pose = Pose(Point(1, 2, 3), Quaternion(0, 0, 0, 1))
        self.assertEqual(initial_pose, test_request.initial_pose)
        self.assertEqual("world", test_request.reference_frame)
        self.assertIn("<cylinder><radius>200.5</radius><length>100</length></cylinder>",
                      test_request.entity_xml)
        self.assertNotIn("<collision>", test_request.entity_xml)
        self.assertIn("<kinematic>0</kinematic>", test_request.entity_xml)
        self.assertIn("<gravity>0</gravity>", test_request.entity_xml)

    def test_robot_pose_monitor(self):
        robot_id = 'marvin'

        def check_robot_pose(_user_data, pose):
            return not ((-1 < pose.position.x < 1) and
                        (-2.5 < pose.position.y < -1.8) and
                        (0 < pose.position.z < 1))

        test = RobotPoseMonitorState(robot_id, check_robot_pose)
        self.assertEqual('/gazebo/model_states', test._topic)
        self.assertEqual(ModelStates, test._msg_type)

        ms = ModelStates([robot_id, "something_else"],
                         [Pose(Point(0, 0, 0.5), Quaternion(0, 0, 0, 0)),
                          Pose(Point(1, -2, 0.5), Quaternion(0, 0, 0, 0))],
                         [Vector3(1.0, 1.0, 1.0), Vector3(1.0, 1.0, 1.0)],
                         [Twist(Vector3(1, 0, 0), Vector3(0, 0, 0.1)),
                          Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))])

        self.assertTrue(test._cond_cb(None, ms))
        ms.pose[0].position.y = -2
        self.assertFalse(test._cond_cb(None, ms))

        ms = ModelStates(["something_else"],
                         [Pose(Point(1, -2, 0.5), Quaternion(0, 0, 0, 0))],
                         [Vector3(1.0, 1.0, 1.0)],
                         [Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))])

        self.assertTrue(test._cond_cb(None, ms))

    def test_robot_twist_monitor(self):
        robot_id = 'marvin'

        def check_robot_twist(_user_data, twist):
            return twist.linear.x <= 30

        test = RobotTwistMonitorState(robot_id, check_robot_twist)
        self.assertEqual('/gazebo/model_states', test._topic)
        self.assertEqual(ModelStates, test._msg_type)

        ms = ModelStates([robot_id, "something_else"],
                         [Pose(Point(1, 0, 0.5), Quaternion(0, 0, 0, 0)),
                          Pose(Point(1, -2, 0.5), Quaternion(0, 0, 0, 0))],
                         [Vector3(1.0, 1.0, 1.0), Vector3(1.0, 1.0, 1.0)],
                         [Twist(Vector3(10, 0, 0), Vector3(0, 0, 0.1)),
                          Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))])

        self.assertTrue(test._cond_cb(None, ms))
        ms.twist[0].linear.x = 100
        self.assertFalse(test._cond_cb(None, ms))

        ms = ModelStates(["something_else"],
                         [Pose(Point(1, -2, 0.5), Quaternion(0, 0, 0, 0))],
                         [Vector3(1.0, 1.0, 1.0)],
                         [Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))])

        self.assertTrue(test._cond_cb(None, ms))

    def test_link_pose_monitor(self):
        def check_link_pose(user_data, pose):
            return not ((-1 < pose.position.x < 1) and
                        (-2.5 < pose.position.y < -1.8) and
                        (0 < pose.position.z < 1))

        test = LinkPoseMonitorState(check_link_pose, "robot::link")
        self.assertEqual('/gazebo/link_states', test._topic)
        self.assertEqual(LinkStates, test._msg_type)

        ms = LinkStates(
            ["robot::link", "something_else::link"],
            [Pose(Point(0, 0, 0.5), Quaternion(0, 0, 0, 0)),
             Pose(Point(1, -2, 0.5), Quaternion(0, 0, 0, 0))],
            [Twist(Vector3(1, 0, 0), Vector3(0, 0, 0.1)),
             Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))],
            [Twist(Vector3(1, 0, 0), Vector3(0, 0, 0.1)),
             Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))],
            [Twist(Vector3(1, 0, 0), Vector3(0, 0, 0.1)), Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))]
        )

        self.assertTrue(test._cond_cb(None, ms))
        ms.pose[0].position.y = -2
        self.assertFalse(test._cond_cb(None, ms))

        ms = LinkStates(["something_else::link"],
                        [Pose(Point(1, -2, 0.5), Quaternion(0, 0, 0, 0))],
                        [Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))],
                        [Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))],
                        [Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))])

        self.assertTrue(test._cond_cb(None, ms))

    warning_message = None

    @patch("hbp_nrp_excontrol.error_cb", _set_warning_message)
    def test_simulation_warning(self):
        warning_state = WarningState("FooBar")
        self.assertEqual("succeeded", warning_state.execute(None))
        self.assertEqual("FooBar", TestStates.warning_message)

    def test_spike_rate_monitor(self):
        test = MonitorSpikeRateState("foo", lambda ud, t, r: t < 2 and r < 10.0)
        self.assertEqual('/monitor/population_rate', test._topic)
        self.assertEqual(SpikeRate, test._msg_type)

        self.assertTrue(test._cond_cb(None, SpikeRate(0.1, 0.0, "foo")))
        self.assertTrue(test._cond_cb(None, SpikeRate(0.1, 8.0, "bar")))
        self.assertFalse(test._cond_cb(None, SpikeRate(1.1, 15.0, "foo")))
        self.assertTrue(test._cond_cb(None, SpikeRate(1.1, 0.0, "bar")))
        self.assertFalse(test._cond_cb(None, SpikeRate(2.1, 8.0, "foo")))
        self.assertTrue(test._cond_cb(None, SpikeRate(2.1, 15.0, "bar")))

    def test_leaky_integrator_alpha_monitor(self):
        test = MonitorLeakyIntegratorAlphaState("foo", lambda ud, t, r: t < 2 and r < 10.0)
        self.assertEqual('/monitor/leaky_integrator_alpha', test._topic)
        self.assertEqual(SpikeRate, test._msg_type)

        self.assertTrue(test._cond_cb(None, SpikeRate(0.1, 0.0, "foo")))
        self.assertTrue(test._cond_cb(None, SpikeRate(0.1, 8.0, "bar")))
        self.assertFalse(test._cond_cb(None, SpikeRate(1.1, 15.0, "foo")))
        self.assertTrue(test._cond_cb(None, SpikeRate(1.1, 0.0, "bar")))
        self.assertFalse(test._cond_cb(None, SpikeRate(2.1, 8.0, "foo")))
        self.assertTrue(test._cond_cb(None, SpikeRate(2.1, 15.0, "bar")))

    def test_leaky_integrator_exp_monitor(self):
        test = MonitorLeakyIntegratorExpState("foo", lambda ud, t, r: t < 2 and r < 10.0)
        self.assertEqual('/monitor/leaky_integrator_exp', test._topic)
        self.assertEqual(SpikeRate, test._msg_type)

        self.assertTrue(test._cond_cb(None, SpikeRate(0.1, 0.0, "foo")))
        self.assertTrue(test._cond_cb(None, SpikeRate(0.1, 8.0, "bar")))
        self.assertFalse(test._cond_cb(None, SpikeRate(1.1, 15.0, "foo")))
        self.assertTrue(test._cond_cb(None, SpikeRate(1.1, 0.0, "bar")))
        self.assertFalse(test._cond_cb(None, SpikeRate(2.1, 8.0, "foo")))
        self.assertTrue(test._cond_cb(None, SpikeRate(2.1, 15.0, "bar")))

    def test_spike_recorder_monitor(self):
        test = MonitorSpikeRecorderState("foo", lambda _, t, spikes: len(spikes) < 2)
        self.assertEqual('/monitor/spike_recorder', test._topic)
        self.assertEqual(SpikeEvent, test._msg_type)
        self.assertTrue(test._cond_cb(None, SpikeEvent(0.1, 42, [SpikeData(0, 0.0)], "foo", "bar", 0, 0)))
        self.assertTrue(test._cond_cb(None, SpikeEvent(0.1, 42, [], "foo", "bar", 0, 0)))
        self.assertTrue(
            test._cond_cb(None, SpikeEvent(1.1, 42, [SpikeData(0, 0.4), SpikeData(8, 0.6)], "bar", "foo", 0, 0)))
        self.assertFalse(
            test._cond_cb(None, SpikeEvent(1.1, 42, [SpikeData(0, 0.4), SpikeData(8, 0.6)], "foo", "bar", 0, 0)))
        self.assertTrue(test._cond_cb(None, SpikeEvent(2.1, 42,
                                                        [SpikeData(0, 1.6), SpikeData(8, 1.7),
                                                         SpikeData(15, 1.8)], "bar", "foo", 0, 0)))
        self.assertFalse(test._cond_cb(None, SpikeEvent(2.1, 42,
                                                       [SpikeData(0, 1.6), SpikeData(8, 1.7),
                                                        SpikeData(15, 1.8)], "foo", "bar",0, 0)))

    @patch("hbp_nrp_excontrol.nrp_states._LifecycleState.clientLogger")
    @patch("hbp_nrp_excontrol.nrp_states._LifecycleState.rospy")
    def test_stop_simulation(self, rospy_mock, logger_mock):

        test = StopSimulation()
        rospy_mock.Publisher.assert_called_once()

        rospy_mock.get_caller_id.return_value = "just a test"
        result = test.execute(None)
        self.assertEqual("succeeded", result)
        rospy_mock.Publisher().publish.assert_called_once_with("just a test", "started", "stopped",
                                                               "stopped")
        rospy_mock.Publisher().unregister.assert_called_once_with()
        logger_mock.info.assert_called_once()

        logger_mock.info.reset_mock()
        rospy_mock.Publisher().publish.side_effect = Exception
        result = test.execute(None)
        self.assertEqual("failed", result)
        logger_mock.info.assert_called()


if __name__ == '__main__':
    unittest.main()
