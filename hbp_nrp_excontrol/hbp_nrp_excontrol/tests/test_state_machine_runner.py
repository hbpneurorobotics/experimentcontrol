# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This file tests the state machine runner
"""

__author__ = 'Georg Hinkel'

import unittest
from unittest.mock import patch, Mock
from hbp_nrp_excontrol import StateMachineRunner as runner
from hbp_nrp_excontrol import python_version_major
from hbp_nrp_excontrol.tests import done_event
from os import path
from testfixtures import log_capture
import logging, os, sys


class TestStateMachineRunner(unittest.TestCase):

    def state_machine(self):
        return "state_machine_mock.py"

    def sm_name(self):
        return "test_id"

    def setUp(self):
        self.rospy_patch = patch("hbp_nrp_excontrol.StateMachineRunner.rospy")
        self.rospy_mock = self.rospy_patch.start()

        dir = path.split(__file__)[0]
        self.instance = runner.StateMachineRunner(self.sm_name(), path.join(dir, self.state_machine()))

        done_event.clear()

    def tearDown(self):
        self.rospy_patch.stop()

    def test_send_error(self):
        self.instance.init()
        self.instance.send_error(msg="Foo bar", error_type="Bar")
        self.assertTrue(self.rospy_mock.Publisher().publish.called)
        error_msg = self.rospy_mock.Publisher().publish.call_args[0][0]
        self.assertEqual("Bar", error_msg.errorType)
        self.assertEqual("Foo bar", error_msg.message)

    def test_loop(self):
        self.instance.init()
        self.instance.initialize_sm(None)
        self.instance.start(None)
        self.rospy_mock.Publisher().publish.reset_mock()
        self.instance.start(None)
        self.assertTrue(self.rospy_mock.Publisher().publish.called_once_with("Cannot start state machine: "
                                                                             "A previous state machine is still running"))
        self.instance.restart(None)
        self.instance.stop(None)

    def test_uninitialized_loop(self):
        self.instance.init()
        self.instance.start(None)
        self.rospy_mock.Publisher().publish.reset_mock()
        self.instance.start(None)
        self.assertTrue(self.rospy_mock.Publisher().publish.called_once_with("Cannot start state machine: "
                                                                             "A previous state machine is still running"))
        self.instance.restart(None)
        self.instance.stop(None)

    def test_exception(self):
        self.instance.init()
        self.instance.initialize_sm(None)
        sm = self.instance._StateMachineRunner__sm

        def raise_exception():
            raise Exception("Foo")
        sm.execute = raise_exception
        self.instance.start(None)
        self.assertTrue(self.rospy_mock.Publisher().publish.called_once_with("Foo"))

    def test_errors(self):
        #exceptions = [SyntaxError("Foo syntax"),
        #              NameError("Foo name"),
        #              AttributeError("Foo attribute")]

        sm_paths = ["state_machine_mock_syntax_error.txt",
                    "state_machine_mock_name_error.txt",
                    "state_machine_mock_attribute_error.txt"]

        def test_error(sm_path):

            dir = os.getcwd()
            instance = runner.StateMachineRunner(self.sm_name(), path.join(dir, sm_path))

            instance.init()
            instance.send_error = Mock()

            outcome = instance.initialize_sm(None)

            # initialize_sm should return [False, _]
            self.assertFalse(outcome[0])

            self.assertTrue(
                self.rospy_mock.Publisher().publish.called_once())
            self.assertTrue(instance.send_error.called)

        for sm_path in sm_paths:
            test_error(sm_path)

    def test_broken(self):
        dir = path.split(__file__)[0]
        self.instance = runner.StateMachineRunner(self.sm_name() + "_empty", path.join(dir, "state_machine_mock_broken.py"))
        self.instance.init()
        success, message = self.instance.initialize_sm(None)
        self.assertFalse(success)


class TestStateMachineRunner2(TestStateMachineRunner):

    def state_machine(self):
        return "state_machine_mock2.py"

    def sm_name(self):
        return "test_id_2"


class TestRunnerSetup(unittest.TestCase):

    LOGGER_NAME = "hbp_nrp_excontrol.StateMachineRunner"

    @log_capture(level=logging.WARNING)
    def test_set_up_logger(self, logcapture):
        runner.set_up_logger('sm_logfile.txt')
        self.assertTrue(os.path.isfile('sm_logfile.txt'))
        os.remove('sm_logfile.txt')

        runner.set_up_logger(None)
        logcapture.check((
            self.LOGGER_NAME,
            'WARNING',
            'Could not write to specified logfile or no logfile specified, logging to stdout now!'
        ))
        self.assertEqual(logging.getLogger().getEffectiveLevel(), logging.INFO)

        runner.set_up_logger(None, True)
        self.assertEqual(logging.getLogger().getEffectiveLevel(), logging.DEBUG)

    @log_capture(level=logging.CRITICAL)
    def test_except_hook(self, logcapture):
        runner.set_up_logger(None)
        sys.excepthook(Exception, Exception("Something really bad happened"), "This is a stacktrace")

        logcapture.check((
            self.LOGGER_NAME,
            'CRITICAL',
            "Unhandled exception of type {exception_str}: Something really bad happened"
                .format(exception_str="<class 'Exception'>" if python_version_major > 2 else "<type 'exceptions.Exception'>")
        ))


if __name__ == '__main__':
    unittest.main()
