"""
This package contains the experiment control for the HBP neurorobotics platform
"""
import sys
from hbp_nrp_excontrol.version import VERSION as __version__  # pylint: disable=W0611

__author__ = "Sebastian Krach, Georg Hinkel"

sim_id = 0
sm_id = None
error_cb = None
python_version_major = sys.version_info.major
