#!/bin/bash

export IGNORE_LINT="platform_venv"
export VIRTUAL_ENV=$NRP_VIRTUAL_ENV

# This script only runs static code analysis, the tests can be run separately using run_tests.sh
make run_pycodestyle run_pylint
